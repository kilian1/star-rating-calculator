import * as request from "request";

export class OsuService {
    getOsuBeatmap(map_id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            const url = "https://osu.ppy.sh/osu/" + map_id;
            request(url, (error, response, body) => {
                if (!error && response.statusCode === 200)
                    resolve(body);
                else
                    reject(error);
            });
        });
    };
};