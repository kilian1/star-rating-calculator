# osu-sr-calculator
Package to calculate star ratings with any mod combination on osu! beatmaps
Using the recently updated star rating algorithm

## Installation
```sh
npm install osu-sr-calculator --save
```
```sh
yarn add osu-sr-calculator
```

## Usage
```typescript
import { calculateStarRating } from 'osu-sr-calculator';

const starRating = await calculateStarRating(mapId, mods, allCombinations, returnAllDifficultyValues);
```

The calculateStarRating function accepts four parameters:
* mapId: the map id of the beatmap (make sure you use the id from the /b url)
* mods (optional): an array containing the mods you want to include in the calculation. Omitting this parameter will default to nomod. Any mod combination can be used, but only HR, EZ, DT and HT will influence the star rating. Any invalid string will be ignored.
* allCombinations (optional): a boolean containing whether or not to calculate all possible star ratings for this map. If set to true, the mods parameter will obviously be ignored.
* returnAllDifficultyValues (optional): a boolean containing whether or not to return the aim and speed ratings as well. Default is false

This function returns an object containing the calculated star rating for the requested mod combination. In case of allCombinations, the object will contain all mod combinations with their corresponding star ratings. 

## Examples

* Nomod star rating:
```typescript
const starRating = await calculateStarRating(1616712);
/* Response:
{ nomod: 4.740512165564775 }
*/
```

* DT star rating:
```typescript
const starRating = await calculateStarRating(1616712, ["DT"]);
/* Response:
{ DT: 6.615083213333153 }
*/
```

* DTHR star rating:
```typescript
const starRating = await calculateStarRating(1616712, ["DT", "HR"]);
/* Response:
{ DTHR: 7.123757009172763 }
*/
```

* All possible star ratings:
```typescript
const starRating = await calculateStarRating(1616712, [], true);
/* Response:
{
    nomod: 4.740512165564775,
    DT: 6.615083213333153,
    HT: 3.8242340076263517,
    HR: 5.104644786105074,
    HRDT: 7.123757009172763,
    HRHT: 4.116661624811091,
    EZ: 4.25926190283471,
    EZDT: 5.94301716616598,
    EZHT: 3.44160529827689
}
*/
```

* Return aim and speed ratings as well
```typescript
const starRating = await calculateStarRating(1616712, [], false, true);
/* Response:
{
    nomod: {
        aim: 2.48419911531009,
        speed: 2.0284269851992796,
        total: 4.740512165564775
    }
}
*/
```

## Accuracy
This package parses the beatmap and calculates the star rating in exactly the same way as in the official osu! source code.
Due to slight difference between C# and TypeScript, a fault margin of at most 0.01 should be expected.
Accuracy on "unrankable" beatmaps can be further off.